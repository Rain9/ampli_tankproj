﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace Scripts.Editor.HotControls{
	public class HotKeys : EditorWindow {
		[MenuItem ("Tools/HotKeys/SwitchObject _`")]
		private static void SwitchObject ()
		{
			GameObject [] objs = Selection.gameObjects;
			for (int i = 0; i < objs.Length; i++)
			{
				objs[i].SetActive (!objs[i].activeInHierarchy);
			}		
		}

		[MenuItem ("Tools/HotKeys/ClearConsole _c")]
		private static void ClearConsole ()
		{
            var logEntries = System.Type.GetType("UnityEditor.LogEntries,UnityEditor.dll");//UnityEditorInternal
            var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            clearMethod.Invoke(null, null);
	    }
	}
}
