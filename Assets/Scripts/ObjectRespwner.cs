﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GrateRandom = UnityEngine.Random;

public class ObjectRespwner : MonoBehaviour {

	#region Params

	public float maxRandom = 0f;

	[SerializeField]
	private GameObject [] objArr;

	#endregion
	void Start () {

		objArr = new GameObject [5];

		for (int i =0; i<objArr.Length; i++)
		{
			objArr [i] = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			objArr [i].transform.position = NewRandomPos (2);
		}
	}
	

	public Vector3 NewRandomPos (float y)
	{
		Vector3 result = GrateRandom.insideUnitCircle * maxRandom;
		return new Vector3 (result.x, y, result.y);
	}



}
