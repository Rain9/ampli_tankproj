﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	#region Params
		private ParticleSystem ps;
		private MeshRenderer mr;
		private bool _run;
		private Vector3 Direction;
		private float speed = 1.1f;
	#endregion

	private void Awake ()
	{
		mr = GetComponent <MeshRenderer>();
		ps = GetComponentInChildren <ParticleSystem>();
	}

	private void OnEnable() {
		mr.enabled = true;
		ps.Clear();	
	}

	/// <summary>
	/// Set projectile move direction
	/// </summary>
	/// <param name="dir">Move direction</param>
	public void Init (Vector3 dir)
	{
		Direction = dir;
		_run = true;
	}
	private void Update() {
		if (_run)
		{
            this.transform.position += this.Direction * speed;
        }
	}

	private IEnumerator Die ()
	{
		yield return new WaitWhile (()=>ps.isPlaying);
		Destroy (this.gameObject);
	}

	private void OnCollisionEnter(Collision col) {
		mr.enabled = false;
		ps.Play (true);
		_run = false;
		StartCoroutine (Die ());
	}

}
