﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour {

#region Params
	[SerializeField]
	private GameObject turret, Gun;

	[SerializeField]
	private bool lookOnTarget;

	[SerializeField]
	private Transform target;
	public float MoveSpeed, RotateSpeed, TurretRotationSpeed;
	public Projectile ProjectilePrefab;

	public Transform ShootPos;

	public static event System.Action shootEvent = delegate {};

#endregion
	
	private void Start ()
	{
		
	}

	void Update () {
		Move ();
		TurretRotation ();
	}

	private void Move ()
	{
		this.transform.Rotate (0,Input.GetAxis("Horizontal") * (Time.deltaTime * RotateSpeed),0);
		this.transform.position = transform.position 
			+ ((transform.forward * Input.GetAxis ("Vertical")) * (Time.deltaTime * MoveSpeed));
	}

	private void TurretRotation ()
	{
        if (Input.GetKeyUp(KeyCode.R))
        {
            lookOnTarget = !lookOnTarget;
        }

		Shoot ();

        Vector3 dir = this.transform.forward;

		if (lookOnTarget)
		{
			if (target != null)
			{
				dir = target.transform.position - this.transform.position;
			}
		}else {
			dir = Camera.main.ScreenPointToRay (Input.mousePosition).direction;
		}
        dir = new Vector3 (dir.x,0,dir.z);
		Quaternion t = Quaternion.LookRotation(dir, Vector3.up);
        turret.transform.rotation = Quaternion.Lerp (turret.transform.rotation
		,t, Time.deltaTime * TurretRotationSpeed);
		
    }

	private void Shoot ()
	{
		if (Input.GetKeyUp (KeyCode.Space))
		{
			Projectile bullet = Instantiate(ProjectilePrefab, ShootPos.position, Quaternion.identity);
			bullet.Init (turret.transform.forward);
		}
	}
}
